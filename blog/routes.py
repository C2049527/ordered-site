from flask import render_template, url_for, redirect
from blog import app
import mysql.connector


mydb = mysql.connector.connect(
    host="csmysql.cs.cf.ac.uk",
    user="c2049527",
    passwd="Fudge-1234",
    database="c2049527_CommerceProject",
    auth_plugin='mysql_native_password'
)

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM pokemon")

myresult = mycursor.fetchall()

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM pokemon")

myresult = mycursor.fetchall()

@app.route("/home", methods=["POST"])
def gohome():
  return redirect(url_for('.home'))


@app.route('/cart')
def goBasket():
  return redirect(url_for('.basket'))

@app.route("/")
def home():
    if "cart" in session:
        items = session["cart"]
        total = session["total"]
        return render_template("Main.html", content=myresult, Current_Cart=items, Price_Total=total, From_add=False)
    else:
        return render_template("Main.html", content=myresult, From_add=False)


@app.route('/basket', methods=["POST"])
def basket():
    if "cart" in session:
        items = session["cart"]
        total = session["total"]
        return render_template("Cart.html", Current_Cart=items, Price_Total=total)
    else:
        return redirect(url_for('.home'))

@app.route('/checkout', methods=["POST"])
def checkout():
    items = session["cart"]
    total = session["total"]
    return render_template("Checkout.html", Current_Cart=items, Price_Total=total)



@app.route('/add', methods=['POST'])
def add_to_cart():
    id = request.form['code']
    getstring = "SELECT * FROM pokemon WHERE idmons= " + str(id)
    mycursor.execute(getstring)
    addedmon = mycursor.fetchone()
    session.modified = True
    if "total" not in session:
        session["total"] = 0

    if "cart" not in session:
        session["cart"] = []

    session["total"] = session["total"] + addedmon[5]
    session["cart"].append(addedmon)

    items = session["cart"]
    total = session["total"]
    return render_template("Main.html", content=myresult, Current_Cart=items, Price_Total=total, From_add=True)


@app.route('/remove', methods=['POST'])
def remove_from_cart():
    id = request.form['code']
    getstring = "SELECT * FROM pokemon WHERE idmons= " + str(id)
    mycursor.execute(getstring)
    removedmon = mycursor.fetchone()
    session.modified = True
    indexofmon = int;
    session["total"] = session["total"] - removedmon[5]
    session["cart"].remove(removedmon)
    if len(session["cart"]) == 0:
      session.clear()
    return redirect(url_for('home'))

@app.route('/removeBasket', methods=['POST'])
def remove_from_basket():
    id = request.form['code']
    getstring = "SELECT * FROM pokemon WHERE idmons= " + str(id)
    mycursor.execute(getstring)
    removedmon = mycursor.fetchone()
    session.modified = True
    indexofmon = int;
    session["total"] = session["total"] - removedmon[5]
    session["cart"].remove(removedmon)
    if len(session["cart"]) == 0:
      session.clear()
    if "cart" in session:
        items = session["cart"]
        total = session["total"]
        return render_template("Cart.html", Current_Cart=items, Price_Total=total)
    else:
        return redirect(url_for('.home'))

@app.route('/goto', methods=['POST'])
def goto_mon():
    id = request.form['code']
    getstring = "SELECT * FROM pokemon WHERE idmons= " + str(id)
    mycursor.execute(getstring)
    mon = mycursor.fetchone()
    session.modified = True
    PageUrl = mon[1] + ".html"
    if "cart" in session:
        items = session["cart"]
        total = session["total"]
        return render_template(PageUrl, Current_Cart=items, Price_Total=total, CurrentMon=mon)
    else:
        return render_template(PageUrl, CurrentMon=mon)